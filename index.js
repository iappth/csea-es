const elasticSearch = require('elasticsearch')
const moment = require('moment')

const client = new elasticSearch.Client({
  host: 'https://crawler-es.iapp.co.th',
  log: 'trace'
})

function checkDate(date) {
  if (date) {
    return moment(date).local('th').startOf('day').format('YYYY-MM-DD HH:mm:ss')
  }
  return null
}

function setFilter(std, ed) {
  let filter = {}
  let format = "YYYY-MM-DD HH:mm:ss"
  if (std && ed) {
    filter = {
      filter: {
        range: {
          written_time: {
            gte: std,
            lte: ed,
            format
          }
        }
      }
    }
  } else if (std) {
    filter = {
      filter: {
        range: {
          written_time: {
            gte: std,
            format
          }
        }
      }
    }
  } else if (ed) {
    filter = {
      filter: {
        range: {
          written_time: {
            lte: ed,
            format
          }
        }
      }
    }
  }

  return filter
}

async function setCondition(data, limit = false) {
  let words = null
  let page = data.page || 1
  let size = data.size || 1000
  let std = checkDate(data.std)
  let ed = checkDate(data.ed)
  let querySize = {}
  let content = {}
  let filter = setFilter(std, ed)
  console.log('std =>', std)
  console.log('ed =>', ed)
  if (Array.isArray(data.q)) {
    words = data.q.map(value => value.text).join(" ")
  } else {
    words = data.q
  }

  content = { content: words }

  if (limit) {
    querySize = {
      from: page * size,
      size
    }
  }

  let body = {
    ...querySize,
    query: {
      bool: {
        must: {
          match: content
        },
        ...filter
      }
    }
  }

  console.log(body)

  let response = await client.search({
    index: 'news',
    body
  })

  let title = response.hits.hits.map(item => item._source.title)

  console.log(title)
}

let mock = {
  q: "ป้อม",
  table: "news_cooked",
  page: 2,
  size: 10,
  std: null,
  ed: new Date("1/01/2018")
}

setCondition(mock, true)
